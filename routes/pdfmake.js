const express = require('express');
const router = express.Router();

const path = require('path');

var fs = require('fs');
var PdfPrinter = require('pdfmake/src/printer');

const pdfMake = require('pdfmake/build/pdfmake');
const vfsFonts = require('pdfmake/build/vfs_fonts');

pdfMake.vfs = vfsFonts.pdfMake.vfs;

router.post('/pdf', (req, res, next) => {
  //res.send('PDF');

  const fname = req.body.fname;
  const lname = req.body.lname;

  function fontPath(file) {
    return path.resolve('public', 'fonts', file);
  }

  var fontDescriptors = {
    Roboto: {
      normal: fontPath('Roboto-Regular.ttf'),
      bold: fontPath('Roboto-Medium.ttf'),
      italics: fontPath('Roboto-Italic.ttf'),
      bolditalics: fontPath('Roboto-Italic.ttf')
    }
  };

  var printer = new PdfPrinter(fontDescriptors);

  var documentDefinition = {
    content: [`Hello ${fname} ${lname}`, 'Nice to meet you!']
  };

  res.set('content-type', 'application/pdf');
  var pdfDoc = printer.createPdfKitDocument(documentDefinition);
  pdfDoc.pipe(fs.createWriteStream('output.pdf'));
  pdfDoc.end();
});

module.exports = router;
